import bos.*;

import java.awt.*;
import java.util.Optional;
import java.util.List;

public class Wolf extends Character {

    public Wolf(Cell location, Behaviour behaviour) {
        super(location, behaviour);
        display = Optional.of(Color.RED);
        description = "wolf";
    }

    @Override
    public void paint(Graphics g) {
        if (display.isPresent()) {
            g.setColor(display.get());
            g.fillOval(location.x + location.width / 4, location.y + location.height / 4, location.width / 2, location.height / 2);
            g.setColor(Color.BLACK);
            g.drawOval(location.x + location.width / 4, location.y + location.height / 4, location.width / 2, location.height / 2);
            g.setColor(Color.BLACK);
            g.drawString("W",location.x+location.height/2-4,location.y+location.height/2+4);
        }
    }
}