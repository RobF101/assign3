import bos.*;

import java.awt.*;
import java.util.*;
import java.util.List;

public class RabbitAdapter extends Character {
    private Rabbit adaptee;

    public RabbitAdapter(Cell location) {
        super(location, null);
        display = Optional.of(Color.GRAY);
        adaptee = new Rabbit();
        description = "rabbit";
    }

    @Override
    public List<RelativeMove> aiMoves(){
        switch (adaptee.nextMove()){
            case 0:
                return Arrays.asList(new MoveDown(Stage.getInstance().grid, this));
            case 1:
                return Arrays.asList(new MoveUp(Stage.getInstance().grid, this));
            case 2:
                return Arrays.asList(new MoveLeft(Stage.getInstance().grid, this));
            case 3:
                return Arrays.asList(new MoveRight(Stage.getInstance().grid, this));
        };
        return Arrays.asList(new MoveRandomly(Stage.getInstance().grid, this));
    }
    @Override
    public void paint(Graphics g) {
        if (display.isPresent()) {
            g.setColor(display.get());
            g.fillOval(location.x + location.width / 4, location.y + location.height / 4, location.width / 2, location.height / 2);
            g.setColor(Color.BLACK);
            g.drawOval(location.x + location.width / 4, location.y + location.height / 4, location.width / 2, location.height / 2);
            g.setColor(Color.BLACK);
            g.drawString("R",location.x+location.height/2-4,location.y+location.height/2+4);
        }
    }
}