import java.awt.*;
import java.util.List;
import java.util.Optional;
import bos.*;

public class Sheep extends Character {

    public Sheep(Cell location, Behaviour behaviour) {
        super(location, behaviour);
        display = Optional.of(Color.WHITE);
        description = "sheep";
    }

    @Override
    public void paint(Graphics g) {
        if (display.isPresent()) {
            g.setColor(display.get());
            g.fillOval(location.x + location.width / 4, location.y + location.height / 4, location.width / 2, location.height / 2);
            g.setColor(Color.BLACK);
            g.drawOval(location.x + location.width / 4, location.y + location.height / 4, location.width / 2, location.height / 2);
            g.setColor(Color.BLACK);
            g.drawString("S",location.x+location.height/2-4,location.y+location.height/2+4);
        }
    }
}