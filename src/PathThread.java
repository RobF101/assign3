import bos.RelativeMove;

import java.util.ArrayList;
import java.util.List;

public class PathThread extends Thread {
    public Character c;
    public List<RelativeMove> m;
    public boolean trucking = true;

    public PathThread(Character ch) {
        c = ch;
        m = new ArrayList<>();
    }

    @Override
    public void run() {
        calcMoves();
    }

    public void calcMoves() {
        this.m.clear();
        this.m.addAll(c.aiMoves());
        System.out.println("CalcMoves");
        System.out.println(m.toString());
    }

    public List<RelativeMove> getMoves() {
        System.out.println("getMoves");
        System.out.println(this.m.toString());
        return this.m;
    }
}